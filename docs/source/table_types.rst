.. _tabletypes:
Types of tables supported by Luna
==================================

Luna supports multiple types of table-versioning schema out of the box.Luna

Dated Tables
------------
These are tables that are versioned through the use of a pair of DATE or DATETIME fields indicating the validity of a record. In this example, these columns are called 'ValidFrom' and 'ValidTo', but these are configurable within Luna.

.. csv-table:: Customers
   :header: "FirstName", "LastName", "SalesRank", "ValidFrom", "ValidTo"
   :widths: 20, 20, 10, 20, 20

   "Kerry", "Scott", 1, "1 Jan 2016", "31 Dec 9999"
   "John", "Smith", 2, "1 Jan 2016", "31 Dec 9999"
   "Mark", "Cavendish", 2, "1 Jan 2016", "1 Jul 2016"
   "Mark", "Cavendish", 3, "1 Jul 2016", "31 Dec 9999"   
   "Christine", "Gumble", 4, "11 Jul 2016", "31 Dec 9999"

Inserting a record into a Dated table will result in the 'ValidFrom/To' columns being set with the current date/time and the MAX date/time: '31 Dec 9999 23:59:59'.

Deleting a record will set the 'ValidTo' field to the current date.

Amending a record will expire the current record and insert a new one with the updated values and new 'ValidFrom/To' values.

BitFlagged Tables
-----------------
These are tales that are versioned through the use of a BIT field. Old/invalid records are set to disabled instead of being deleted. In this example, the BIT column is called IsActive, and again, this is configurable in Luna.

.. csv-table:: Customers
   :header: "FirstName", "LastName", "SalesRank", "IsActive"
   :widths: 20, 20, 10, 20

   "Kerry", "Scott", 1, "True"
   "John", "Smith", 2, "True"
   "Mark", "Cavendish", 2, "False"
   "Mark", "Cavendish", 3, "True"   
   "Christine", "Gumble", 4, "True"

Inserting a record into a BitFlagged table will result in the 'IsActive' flag being set to TRUE (1).

Deleting a record while set the flag to FALSE (0), while amending a record will set the flag on the current record to FALSE and create a copy with the new values and the flag set to TRUE.

Unflagged Tables
----------------
These are unversioned tables. I.e. there is no special mechanism for maintaining validity of the data. Operations on these tables update and delete in place, so no history is preserved.

.. csv-table:: Customers
   :header: "FirstName", "LastName", "SalesRank"
   :widths: 20, 20, 10

   "Kerry", "Scott", 1
   "John", "Smith", 2
   "Mark", "Cavendish", 3
   "Christine", "Gumble", 4
   
Operations on an Unflagged table will directly manipulate the data in the table. Amends will change the row in place, while deletions will remove the record from the table.

For this reason, it is advised to carefully consider your usage of Unflagged tables.