.. ArkSigma.Luna documentation master file, created by
   sphinx-quickstart on Fri Jul 29 23:34:19 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to documentation for Luna by ArkSigma!
===============================================

Luna is the static data manager built for SQL Server. It can be configured with your current SQL Server installation and provides a simple interface to allow your users to manage both versioned and unversioned data tables.

`<https://www.arksigma.com/>`_

Contents:

.. toctree::
   :maxdepth: 2

* :ref:`install-docs`
* :ref:`configuration-docs`
* :ref:`user-docs`

.. _install-docs:

.. toctree::
   :maxdepth: 2
   :caption: Installation

   getting_started
   http_configuration
   ssl_configuration

.. _configuration-docs:

.. toctree::
   :maxdepth: 2
   :caption: Configuration
   
   table_types
   configuring
   glossary
   
.. _user-docs:

.. toctree::
   :maxdepth: 2
   :caption: User Manual
   
   user_interface