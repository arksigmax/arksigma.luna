Configuring Luna for initial use
================================

After you login for the first time, you will be presented with the main Luna interface. There's not alot to see here, so click on the gears in the top-right corner, and hit 'Configure' to be taken to the configuration screen.

.. image:: images/home-screen.jpg


Creating your first Environment
-------------------------------

The first thing to do is to create an Environment. Click on the '+' symbol on environment pane to open the new environment dialog.The

.. image:: images/environment-screen.jpg

Enter a name for the environment and select a colour. This will define how the environment is displayed on the main Luna screen.Enter

Hit 'Save' when you're happy with your environment.


Configuring a Server
--------------------

Once you have an environment, you can configure a Server by clicking the '+' symbol on the Server pane.

Luna supports two types of authentication on the server, outlined below. Once you have entered the details, you can click 'Test Connection' to verify the details and initiate a connection to the database. Once this is successful, hit 'Save' to save your settings and close the dialog.

Integrated Authentication
^^^^^^^^^^^^^^^^^^^^^^^^^

When configured in this mode, Luna will authenticate against the server using the integrated user, i.e. the account the Luna service is running as in Windows. This is also known as 'Windows Authentication' in SQL Server.

Server Authentication
^^^^^^^^^^^^^^^^^^^^^

When using server authentication, Luna allows you to enter a Username/Password combo to authenticate with. This is also known as 'SQL Server Authentication'.

Adding a Table
--------------

Now we can configure our first table in Luna. Hit the '+' symbol on the Tables pane to bring up the new table dialog. The 'Database' dropdown should automatically populate based on the server, so proceed to fill out the details requested.Now

The form will ask for the Primary Key field on the table to support key-based operations on the table.

.. image:: images/table-dialog.jpg

Once you have filled out the table information, proceed to select the type of table. Valid options are Dated, BitFlagged or Unflagged. These are explained in the section `Types of tables supported by Luna`.

Selection of the table type will enable selection of additional fields relevant to your selection.

The final part of configuring a table is the option to add it to a Luna Group. This will enable quick visibility in Luna as a table will not be visible to anyone without being a member of a Group. Select an existing Group from the list or choos <Add to new Group> to create a new one.The

Hit 'Save' to save your changes and close the dialog.

Adding Linked Tables
--------------------

After you have configured multiple tables, you can proceed to defining relationships between them. In SQL Server, this is also known as 'Foreign Key Relationships'. With the table highlighted in the tables pane, hit the '+' symbol on the Linked Tables pane to bring up the dialog. The form requires you to define which table to link the currently selected one to, and which fields to use as joining keys.

Once you have defined your linked tables, you can add some 'Linked Table Columns'. This defines which columns are visible from the linked table when viewing the primary table in Luna. You can create Linked Tables in either direction to support your needs.
