Getting Started
================

Installation
------------

After download, run the MSI installer. You may need Administrative permissions to do so.

.. image:: images/install-dialog.jpg

By default, Luna installs in your 'Program Files' directory, usually on your 'C' drive, although this may be different depending on your setup. From here on, we'll assume your installation directory is 'C:\\Program Files\\ArkSigma\\'

.. image:: images/install-dir.jpg

Once the installation is complete, the installer will install and start the service and make it accessible at the following address in your browser:

http://localhost:50001

Luna will also be accessible if you replace the 'localhost' with the machine name. E.g. http://<MACHINE_NAME>:50001

Logging in for the first time
-----------------------------

.. image:: images/login-screen.jpg

When you first launch Luna, you will be presented with a login screen, requesting you use your Windows credentials. 

Luna will automatically authenticate with your Domain Controller and fallback to your machine if you are not a member of a domain.

This first login is crucial to Luna as this user will be setup as the first Luna administrator.

Once login is successful, you will be taken to the main Luna page.

